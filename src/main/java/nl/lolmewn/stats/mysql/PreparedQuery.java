package nl.lolmewn.stats.mysql;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Lolmewn
 */
public class PreparedQuery {
    
    private List<String> whereRows = new ArrayList<String>();
    private List<Object> whereValues = new ArrayList<Object>();
    private String updateRow;
    private int updateValue;
    private String tableName;
    private boolean setAddMode;
    
    public PreparedQuery(String table){
        this.tableName = table;
    }
    
    public PreparedQuery addWhere(String row, Object value){
        this.whereRows.add(row);
        this.whereValues.add(value);
        return this;
    }
    
    public PreparedQuery setUpdateRow(String rowName){
        this.updateRow = rowName;
        return this;
    }
    
    public PreparedQuery setUpdateValue(int value){
        this.updateValue = value;
        return this;
    }
    
    public PreparedQuery setAddMode(boolean value){
        this.setAddMode = value;
        return this;
    }
    
    public String getPreparedUpdateStatement(){
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ").append(this.tableName).append(" SET ");
        sb.append(this.updateRow).append("=");
        if(this.setAddMode){
            sb.append(this.updateRow).append("+");
        }
        sb.append(this.updateValue);
        sb.append(" WHERE ");
        for(int i = 0; i < this.whereRows.size(); i++){
            if(i != 0){
                sb.append(" AND ");
            }
            sb.append(this.whereRows.get(i)).append("=?");
        }
        return sb.toString();
    }
    
    public Object[] getPreparedUpdateValues(){
        return this.whereValues.toArray(new Object[this.whereValues.size()]);
    }

}
