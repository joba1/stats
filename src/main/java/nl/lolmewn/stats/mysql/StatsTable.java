/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.mysql;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public enum StatsTable {
    
    PLAYER("player"),
    BLOCK("block"),
    MOVE("move"),
    KILL("kill"),
    DEATH("death");

    private String name;
    
    private StatsTable(String name) {
        this.name = name;
    }
    
    @Override
    public String toString(){
        return this.name;
    }

}
