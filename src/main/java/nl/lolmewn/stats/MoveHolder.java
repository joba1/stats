/*
 *  Copyright 2012 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats;

import java.util.HashMap;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class MoveHolder {
    
    private HashMap<Integer, Double> distances = new HashMap<Integer, Double>();
    
    public HashMap<Integer, Double> getDistances(){
        return this.distances;
    }
    
    public void clear(){
        this.distances.clear();
    }
    
    public void addDistance(double dist, int type){
        distances.put(type, distances.containsKey(type) ? distances.get(type) + dist : dist);
    }

    public double getDistance(int type) {
        return distances.containsKey(type) ? distances.get(type) : 0;
    }
    
}
