/*
 *  Copyright 2012 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class QueryHolder implements Serializable{
    private static final long serialVersionUID = 134542574568L;

    private String insert;
    private String sql;
    private Object[] vars;
    
    public QueryHolder(String sql, String insert, Object... variables){
        this.sql = sql;
        this.insert = insert;
        this.vars = variables;
    }

    public String getInsert() {
        return insert;
    }

    public String getUpdate() {
        return sql;
    }
    
    public int executeSQL(Connection con){
        if(this.sql == null){
            return -1;
        }
        try {
            PreparedStatement st = con.prepareStatement(sql);
            for(int position = 1; position <= this.vars.length; position++){
                st.setObject(position, vars[position -1]);
            }
            int returned = st.executeUpdate();
            st.close();
            return returned;
        } catch (SQLException ex) {
            System.out.println("Exception happened while executing SQL: " + ex.getMessage());
            //System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("Query: " + sql + "; param: " + Arrays.toString(vars));
            ex.printStackTrace();
        }
        
        return 0;
    }
    
    public Object[] getVars(){
        return vars;
    }

    QueryHolder makeReadyForGlobal(String dbPrefix) {
        if(insert != null){
            insert = insert.replace(dbPrefix, "$$PREFIX");
        }
        if(sql != null){
            sql = sql.replace(dbPrefix, "$$PREFIX");
        }
        return this;
    }

}
