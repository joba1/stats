package nl.lolmewn.stats;

import org.bukkit.configuration.file.FileConfiguration;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Settings {

    private Main plugin;
    private String dbHost;
    private String dbPass;
    private String dbUser;
    private String dbName;
    private String dbPrefix;
    private int dbPort;
    
    private boolean ignoreCreative;
    
    private boolean debug;
    private boolean update;
    
    private boolean sendToGlobal;
    
    private Main getPlugin(){
        return this.plugin;
    }
    
    public Settings(Main m) {
        this.plugin = m;
    }

    protected void loadSettings() {
        FileConfiguration f = this.getPlugin().getConfig();
        this.dbUser = f.getString("MySQL-User", "root");
        this.dbPass = f.getString("MySQL-Pass", "root");
        this.dbHost = f.getString("MySQL-Host", "localhost");
        this.dbPort = f.getInt("MySQL-Port", 3306);
        this.dbName = f.getString("MySQL-Database", "minecraft");
        this.dbPrefix = f.getString("MySQL-Prefix", "Stats_");
        this.debug = f.getBoolean("debug", false);
        this.sendToGlobal = f.getBoolean("sendStatsToGlobalServer", true);
        this.update = f.getBoolean("update");
        this.ignoreCreative = f.getBoolean("ignoreCreative", false);
    }
    
    public void setDebugging(boolean value){
        this.debug = value;
    }
    
    public boolean isDebugging(){
        return this.debug;
    }

    protected String getDbHost() {
        return dbHost;
    }

    protected String getDbName() {
        return dbName;
    }

    protected String getDbPass() {
        return dbPass;
    }

    protected int getDbPort() {
        return dbPort;
    }

    public String getDbPrefix() {
        return dbPrefix;
    }

    protected String getDbUser() {
        return dbUser;
    }

    public boolean isSendToGlobal() {
        return sendToGlobal;
    }

    public boolean isUpdate() {
        return update;
    }

    public boolean isIgnoreCreative() {
        return ignoreCreative;
    }
    
}
