/*
*  Copyright 2013 Lolmewn <info@lolmewn.nl>.
*/

package nl.lolmewn.stats;

/**
*
* @author Lolmewn <info@lolmewn.nl>
*/
public class VarHolder {

    protected Object[] vars = new Object[0];
    protected String insert;
    
    protected double updateValue = 0;

    public VarHolder(Object[] vars, String insert){
        this.vars = vars;
        this.insert = insert;
    }
    
    public VarHolder addToUpdate(double value){
        this.updateValue += value;
        return this;
    }
    
    public VarHolder setObjects(Object[] vars){
        this.vars = vars;
        return this;
    }

}
