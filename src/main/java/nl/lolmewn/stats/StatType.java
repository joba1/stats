package nl.lolmewn.stats;

/**
 * @author Lolmewn
 */
public enum StatType {
    
    BLOCK_BREAK("UPDATE PREFIXblock SET amount=amount+? WHERE player=? AND blockID=? AND blockData=? AND breaking=?",
            "INSERT INTO PREFIXblock (player, blockID, blockData, breaking, amount) VALUES (?, ?, ?, ?, ?)", 5),
    BLOCK_PLACE("UPDATE PREFIXblock SET amount=amount+? WHERE player=? AND blockID=? AND blockData=? AND breaking=?",
            "INSERT INTO PREFIXblock (player, blockID, blockData, breaking, amount) VALUES (?, ?, ?, ?, ?)", 5),
    MOVE("UPDATE PREFIXmove SET distance=distance+? WHERE player=? AND type=?", 
            "INSERT INTO PREFIXmove (player, type, distance) VALUES (?, ?, ?)", 3),
    KILL("UPDATE PREFIXkill SET amount=amount+? WHERE player=? AND type=?",
            "INSERT INTO PREFIXkill (player, time, amount) VALUES (?, ?, ?)", 3),
    DEATH("UPDATE PREFIXdeath SET amount=amount+? WHERE player=? AND cause=? AND entity=?", 
            "INSERT INTO PREFIXdeath (player, cause, entity, amount) VALUES (?, ?, ?, ?)", 4),
    PLAYTIME("UPDATE PREFIXplayer SET playtime=playtime+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, playtime) VALUES (?, ?)", 2),
    ARROWS("UPDATE PREFIXplayer SET arrows=arrows+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, arrows) VALUES (?, ?)", 2),
    XP_GAINED("UPDATE PREFIXplayer SET xpgained=xpgained+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, xpgained) VALUES (?, ?)", 2),
    JOINS("UPDATE PREFIXplayer SET joins=joins+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, joins) VALUES (?, ?)", 2),
    FISH_CATCHED("UPDATE PREFIXplayer SET fishcatch=fishcatch+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, fishcatch) VALUES (?, ?)", 2),
    DAMAGE_TAKEN("UPDATE PREFIXplayer SET damagetaken=damagetaken+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, damagetaken) VALUES (?, ?)", 2),
    TIMES_KICKED("UPDATE PREFIXplayer SET timeskicked=timeskicked+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, timeskicked) VALUES (?, ?)", 2),
    TOOLS_BROKEN("UPDATE PREFIXplayer SET toolsbroken=toolsbroken+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, toolsbroken) VALUES (?, ?)", 2),
    EGGS_THROWN("UPDATE PREFIXplayer SET eggsthrown=eggsthrown+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, eggsthrown) VALUES (?, ?)", 2),
    ITEMS_CRAFTED("UPDATE PREFIXplayer SET itemscrafted=itemscrafted+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, itemscrafted) VALUES (?, ?)", 2),
    OMNOMNOM("UPDATE PREFIXplayer SET omnomnom=omnomnom+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, omnomnom) VALUES (?, ?)", 2),
    ON_FIRE("UPDATE PREFIXplayer SET onfire=onfire+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, onfire) VALUES (?, ?)", 2),
    WORDS_SAID("UPDATE PREFIXplayer SET wordssaid=wordssaid+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, wordssaid) VALUES (?, ?)", 2),
    COMMANDS_DONE("UPDATE PREFIXplayer SET commandsdone=commandsdone+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, commandsdone) VALUES (?, ?)", 2),
    VOTES("UPDATE PREFIXplayer SET votes=votes+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, votes) VALUES (?, ?)", 2),
    LASTJOIN("UPDATE PREFIXplayer SET lastjoin=lastjoin+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, lastjoin) VALUES (?, ?)", 2),
    LASTLEAVE("UPDATE PREFIXplayer SET lastleave=lastleave+? WHERE player=?", 
            "INSERT INTO PREFIXplayer (player, lastleave) VALUES (?, ?)", 2);
    
    private String update, insert;
    private int variableCount;
    
    private StatType(String query, String insert, int variableCount){
        this.update = query;
        this.insert = insert;
        this.variableCount = variableCount;
    }
    
    public String getUpdateStatement(String dbPrefix){
        return update.replaceFirst("PREFIX", dbPrefix);
    }
    
    public String getInsertStatement(String dbPrefix){
        return insert.replaceFirst("PREFIX", dbPrefix);
    }
    
    public int getVariableCount(){
        return this.variableCount;
    }

}
