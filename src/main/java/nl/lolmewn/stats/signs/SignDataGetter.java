/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */
package nl.lolmewn.stats.signs;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import org.bukkit.ChatColor;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class SignDataGetter implements Runnable {

    private Main plugin;
    protected ConcurrentHashMap<String, String[]> toUpdate = new ConcurrentHashMap<String, String[]>();

    public SignDataGetter(Main plugin) {
        this.plugin = plugin;
    }

    @Override
    public void run() {
        if(plugin.newConfig){
            return;
        }
        if(plugin.getMySQL() == null || plugin.getMySQL().isFault()){
            return;
        }
        Connection con = plugin.getMySQL().getConnection();
        for (StatsSign sign : plugin.getSignManager().getAllSigns()) {
            String query;
            ResultSet set = null;
            switch (sign.getSignDataType()) {
                case BLOCKS_BROKEN:
                    query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "block" + (sign.getSignType() == SignType.GLOBAL ? " WHERE break=1" : " WHERE player LIKE ? AND break=1");
                    set = this.getResultSet(con, query, sign.getSignType() == SignType.PLAYER ? new Object[]{sign.getVariable() + "%"} : null);
                    break;
                case BLOCKS_PLACED:
                    query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "block" + (sign.getSignType() == SignType.GLOBAL ? " WHERE break=0" : " WHERE player LIKE ? AND break=0");
                    set = this.getResultSet(con, query, sign.getSignType() == SignType.PLAYER ? new Object[]{sign.getVariable() + "%"} : null);
                    break;
                case DEATHS:
                    query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "death" + (sign.getSignType() == SignType.GLOBAL ? " " : " WHERE player LIKE ?");
                    set = this.getResultSet(con, query, sign.getSignType() == SignType.PLAYER ? new Object[]{sign.getVariable() + "%"} : null);
                    break;
                case KILLS:
                    query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "kill" + (sign.getSignType() == SignType.GLOBAL ? " " : " WHERE player LIKE ?");
                    set = this.getResultSet(con, query, sign.getSignType() == SignType.PLAYER ? new Object[]{sign.getVariable() + "%"} : null);
                    break;
                case PLAYTIME:
                    query = "SELECT SUM(playtime) as result FROM " + plugin.getSettings().getDbPrefix() + "player" + (sign.getSignType() == SignType.GLOBAL ? " " : " WHERE player LIKE ?");
                    set = this.getResultSet(con, query, sign.getSignType() == SignType.PLAYER ? new Object[]{sign.getVariable() + "%"} : null);
                    break;
                case PVP_KILLS:
                    query = "SELECT SUM(amount) as result FROM " + plugin.getSettings().getDbPrefix() + "kill" + (sign.getSignType() == SignType.GLOBAL ? " WHERE type=?" : " WHERE player LIKE ? AND type=?");
                    set = this.getResultSet(con, query, sign.getSignType() == SignType.PLAYER ? new String[]{sign.getVariable() + "%", "Player"} : new Object[]{"Player"});
                    break;
                case CUSTOM:
                    if (sign.getVariable().equals("UNSET")) {
                        continue;
                    }
                    set = this.getResultSet(con, sign.getVariable(), sign.getSignType() == SignType.PLAYER ? (Object[]) new String[]{sign.getVariable(), "Player"} : null);
                    break;
                default:
                    sign.updateSign(ChatColor.BLACK + "[" + ChatColor.YELLOW + "Stats" + ChatColor.BLACK + "]",
                            sign.getSignDataType().toString().substring(0, 1).toUpperCase() + sign.getSignDataType().toString().substring(1).toLowerCase(),
                            sign.getSignType() == SignType.GLOBAL ? "in total" : "by " + sign.getVariable(),
                            ChatColor.RED + "Error occured");
                    return;
            }
            String[] array = new String[4];
            array[0] = ChatColor.BLACK + "[" + ChatColor.YELLOW + "Stats" + ChatColor.BLACK + "]";
            array[1] = sign.getSignDataType() == SignDataType.CUSTOM ? sign.getSignLine() : sign.getSignLine().substring(0, 1).toUpperCase() + sign.getSignLine().substring(1).toLowerCase();
            array[2] = sign.getSignType() == SignType.GLOBAL ? "in total" : "by " + sign.getVariable();
            try {
                if (set != null && set.next()) { 
                    if(set.getObject(1) == null){
                        array[3] = ChatColor.RED + "Non-existing";
                    }else if (set.getString(1).equalsIgnoreCase("")) {
                        array[3] = "None!";
                    } else {
                        if (sign.getSignDataType().equals(SignDataType.PLAYTIME)) {
                            int playTimeSeconds = set.getInt(1);
                            array[3] = String.format("%dd %dh %dm %ds",
                                    TimeUnit.SECONDS.toDays(playTimeSeconds),
                                    TimeUnit.SECONDS.toHours(playTimeSeconds) - TimeUnit.SECONDS.toDays(playTimeSeconds) * 24,
                                    TimeUnit.SECONDS.toMinutes(playTimeSeconds) - TimeUnit.SECONDS.toHours(playTimeSeconds) * 60,
                                    TimeUnit.SECONDS.toSeconds(playTimeSeconds) - TimeUnit.SECONDS.toMinutes(playTimeSeconds) * 60);
                        } else {
                            array[3] = set.getString(1);
                        }
                    }
                } else {
                    array[3] = "Error :O";
                }
            } catch (SQLException ex) {
                Logger.getLogger(SignDataGetter.class.getName()).log(Level.SEVERE, null, ex);
                array[3] = "Error :O";
            }
            this.toUpdate.put(sign.getLocationString(), array);
        }
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(SignDataGetter.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            plugin.getServer().getScheduler().runTaskLater(plugin, new SignUpdaterThread(this), 1);
        }
    }

    protected Main getPlugin() {
        return plugin;
    }

    private ResultSet getResultSet(Connection con, String query, Object[] variables) {
        try {
            PreparedStatement st = con.prepareStatement(query);
            if (variables != null) {
                if (variables.length != 1 || variables[0] != null) {
                    for (int i = 0; i < variables.length; i++) {
                        st.setObject(i + 1, variables[i]);
                    }
                }

            }
            ResultSet set = st.executeQuery();
            return set;
        } catch (SQLException ex) {
            Logger.getLogger(SignDataGetter.class.getName()).log(Level.SEVERE, null, ex);
        }

        return null;
    }
}
