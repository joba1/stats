/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.signs;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.metadata.FixedMetadataValue;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class SignManager {

    private Main plugin;
    private ConcurrentHashMap<String, StatsSign> signs = new ConcurrentHashMap<String, StatsSign>();
    private File signFile = null;
    private YamlConfiguration c = null;
    
    public SignManager(Main plugin) {
        this.plugin = plugin;
        signFile = new File(plugin.getDataFolder(), "signs.yml");
        c = YamlConfiguration.loadConfiguration(signFile);
    }
    
    public void addSign(StatsSign sign, String locationString){
        this.signs.put(locationString, sign);
    }
    
    public void addSign(StatsSign sign, Location loc){
        this.addSign(sign, loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ());
    }
    
    public StatsSign getSignAt(Location loc){
        if(this.signs.containsKey(loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ())){
            return signs.get(loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ());
        }
        return null;
    }
    
    public Collection<StatsSign> getAllSigns(){
        return signs.values();
    }
    
    public void removeSign(Location loc){
        if(this.signs.containsKey(loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ())){
            this.signs.remove(loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ());
            c.set(loc.getWorld().getName() + "," + loc.getBlockX() + "," + loc.getBlockY() + "," + loc.getBlockZ(), null);
            try {
                c.save(signFile);
            } catch (IOException ex) {
                Logger.getLogger(SignManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    
    public void save(){
        if(!signFile.exists()){
            signFile.getParentFile().mkdirs();
            try {
                signFile.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(SignManager.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        for(String loc : signs.keySet()){
            StatsSign sign = signs.get(loc);
            c.set(loc + ".type", sign.getSignType().toString());
            c.set(loc + ".dataType", sign.getSignDataType().toString());
            if(sign.hasVariable()){
                c.set(loc + ".var", sign.getVariable());
            }
            if(sign.getSignDataType().equals(SignDataType.CUSTOM)){
                c.set(loc + ".line", sign.getSignLine());
            }
        }
        try {
            c.save(signFile);
        } catch (IOException ex) {
            Logger.getLogger(SignManager.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void load(){
        if(!signFile.exists()){
            return;
        }
        for(String key : c.getConfigurationSection("").getKeys(false)){
            String[] chop = key.split(",");
            if(chop[0] != null && plugin.getServer().getWorld(chop[0]) != null){
                Location loc = new Location(plugin.getServer().getWorld(chop[0]), 
                    Integer.parseInt(chop[1]), 
                    Integer.parseInt(chop[2]), 
                    Integer.parseInt(chop[3]));
                loc.getBlock().setMetadata("statssign", new FixedMetadataValue(this.plugin, true));
                StatsSign sign = new StatsSign(key, c.getConfigurationSection(key));
                this.addSign(sign, key);
            }
        }
    }

}
