/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.signs;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.ConfigurationSection;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatsSign {

    private SignDataType signDataType;
    private SignType signType;
    private final String world;
    private final int x, y, z;
    private String variable;
    private String signLine;
    
    public StatsSign(SignType type, SignDataType dataType, String world, int x, int y, int z) {
        this.signDataType = dataType;
        this.signType = type;
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
    }
    
    public StatsSign(String location, ConfigurationSection section){
        String[] split = location.split(",");
        world = split[0]; x = Integer.parseInt(split[1]); y = Integer.parseInt(split[2]); z = Integer.parseInt(split[3]);
        signType = SignType.fromString(section.getString("type"));
        signDataType = SignDataType.fromString(section.getString("dataType"));
        if(section.isSet("var")){
            this.variable = section.getString("var");
        }
        this.signLine = section.getString("line", null);
    }
    
    public boolean isActive(){
        World w = Bukkit.getWorld(world);
        if(w == null){
            return false;
        }
        if(!w.getChunkAt(x, z).isLoaded()){
            return false;
        }
        Block b = w.getBlockAt(x, y, z);
        if(!b.getType().equals(Material.SIGN_POST) && !b.getType().equals(Material.WALL_SIGN)){
            return false; 
        }
        if(b.hasMetadata("statssign")){
            return true;
        }
        return false;
    }
    
    public SignDataType getSignDataType(){
        return signDataType;
    }
    
    public SignType getSignType(){
        return signType;
    }
    
    public void updateSign(String line1, String line2, String line3, String line4){
        if(!isActive()){
            return;
        }
        Sign s = (Sign)Bukkit.getWorld(world).getBlockAt(x, y, z).getState();
        s.setLine(0, line1);
        s.setLine(1, line2);
        s.setLine(2, line3);
        s.setLine(3, line4);
        s.update();
    }
    
    public String getLocationString(){
        return world + "," + x + "," + y + "," + z;
    }

    public void setVariable(String var) {
        this.variable = var;
    }
    
    public boolean hasVariable(){
        return this.variable != null;
    }
    
    public String getVariable(){
        return this.variable;
    }

    public String getSignLine() {
        if(this.signDataType.equals(SignDataType.CUSTOM)){
            return signLine;
        }
        return signDataType.toString();
    }

    /*
     * Only used for <code>SignDataType.CUSTOM</code>
     */
    public void setSignLine(String signLine) {
        this.signLine = signLine;
    }

}
