/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.signs;

import java.util.Iterator;
import nl.lolmewn.stats.signs.events.StatsSignUpdateEvent;
import org.bukkit.Bukkit;
import org.bukkit.Location;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class SignUpdaterThread implements Runnable{

    private SignDataGetter source;
    
    public SignUpdaterThread(SignDataGetter from) {
        source = from;
    }

    @Override
    public void run() {
        for (Iterator<String> it = source.toUpdate.keySet().iterator(); it.hasNext();) {
            String location = it.next();
            String[] values = source.toUpdate.get(location);
            it.remove();
            String[] chop = location.split(",");
            StatsSign sign = source.getPlugin().getSignManager()
                    .getSignAt(new Location(Bukkit.getWorld(chop[0]), 
                    Integer.parseInt(chop[1]), 
                    Integer.parseInt(chop[2]), 
                    Integer.parseInt(chop[3])));
            if(sign == null){
                source.getPlugin().getLogger().warning("StatsSign not found at " + location + ", cancelling...");
                continue;
            }
            StatsSignUpdateEvent event = new StatsSignUpdateEvent(sign, values);
            source.getPlugin().getServer().getPluginManager().callEvent(event);
            if(event.isCancelled()){
                return;
            }
            sign.updateSign(values[0], values[1], values[2], values[3]);
        }
    }

}
