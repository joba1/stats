/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.signs;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.signs.events.StatsSignCreateEvent;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class SignListener implements Listener{

    private Main plugin;
    private HashMap<String, Location> customSetters = new HashMap<String, Location>();
    private HashMap<String, Location> signLineSettings = new HashMap<String, Location>();
    
    public SignListener(Main m) {
        this.plugin = m;
    }
    
    @EventHandler
    public void onSignChange(SignChangeEvent event){
        if(!event.getLine(0).equalsIgnoreCase("[Stats]")){
            return;
        }
        if(!event.getPlayer().hasPermission("stats.sign.place")){
            event.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to place a stats sign!");
            event.setCancelled(true);
            event.getBlock().setTypeId(0);
            event.getPlayer().getInventory().addItem(new ItemStack(Material.SIGN, 1));
            return;
        }
        if(!event.getLine(2).equalsIgnoreCase("global") && !event.getLine(2).equalsIgnoreCase("player")){
            event.setCancelled(true);
            event.getBlock().setTypeId(0);
            event.getPlayer().getInventory().addItem(new ItemStack(Material.SIGN, 1));
            event.getPlayer().sendMessage(ChatColor.RED + "Only signs of type 'global' or 'player' allowed");
            return;
        }
        SignType type;
        if(event.getLine(2).equalsIgnoreCase("global")){
            type = SignType.GLOBAL;
        }else{
            if(event.getLine(3).equals("")){
                event.setCancelled(true);
                event.getBlock().setTypeId(0);
                event.getPlayer().getInventory().addItem(new ItemStack(Material.SIGN, 1));
                event.getPlayer().sendMessage(ChatColor.RED + "Last line should be a player name");
                return;
            }
            type = SignType.PLAYER;
        }
        SignDataType dataType = null;
        for(SignDataType available : SignDataType.values()){
            if(available.toString().equals(event.getLine(1))){
                dataType = available;
                break;
            }
        }
        if(dataType == null){
            event.setCancelled(true);
            event.getBlock().setTypeId(0);
            event.getPlayer().getInventory().addItem(new ItemStack(Material.SIGN, 1));
            event.getPlayer().sendMessage(ChatColor.RED + "DataType not recognized. Please use an appropriate one.");
            return;
        }
        StatsSign sign = new StatsSign(type,
                dataType,
                event.getBlock().getWorld().getName(),
                event.getBlock().getX(),
                event.getBlock().getY(),
                event.getBlock().getZ());
        if(type == SignType.PLAYER){
            sign.setVariable(event.getLine(3));
        }
        
        StatsSignCreateEvent createEvent = new StatsSignCreateEvent(sign);
        this.plugin.getServer().getPluginManager().callEvent(createEvent);
        if(createEvent.isCancelled()){
            return;
        }
        
        if(dataType.equals(SignDataType.CUSTOM)){
            event.getPlayer().sendMessage(ChatColor.GREEN + "Please say the SQL Query to get the data from in chat");
            this.customSetters.put(event.getPlayer().getName(), event.getBlock().getLocation());
            sign.setVariable("UNSET");
        }
        plugin.getSignManager().addSign(sign, event.getBlock().getLocation());
        event.getBlock().setMetadata("statssign", new FixedMetadataValue(this.plugin, true));
        
        event.setLine(1, ChatColor.YELLOW + "This sign is");
        event.setLine(2, ChatColor.YELLOW + "pending for");
        event.setLine(3, ChatColor.YELLOW + "data updates...");
    }
    
    @EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
    public void chat(AsyncPlayerChatEvent event) {
        if (this.customSetters.containsKey(event.getPlayer().getName())) {
            try {
                Connection con = plugin.getMySQL().getConnection();
                Statement st = con.createStatement();
                st.executeQuery(event.getMessage());
                st.close();
                con.close();
                this.signLineSettings.put(event.getPlayer().getName(), this.customSetters.get(event.getPlayer().getName()));
                this.customSetters.remove(event.getPlayer().getName());
                event.setCancelled(true);
                StatsSign sign = plugin.getSignManager().getSignAt(this.signLineSettings.get(event.getPlayer().getName()));
                sign.setVariable(event.getMessage());
                sign.setSignLine(ChatColor.RED + "Placeholder");
                event.getPlayer().sendMessage(ChatColor.GREEN + "Please set line two for this sign now by saying it in chat.");
                return;
            } catch (SQLException ex) {
                Logger.getLogger(SignListener.class.getName()).log(Level.SEVERE, null, ex);
                this.plugin.getLogger().severe(ChatColor.RED + "The exeption above was generated by someone entering a faulty SQL query to the Stats plugin, please do NOT report it");
                event.getPlayer().sendMessage(ChatColor.RED + "Faulty SQL query, check the logs and try again.");
            }
        }
        if(this.signLineSettings.containsKey(event.getPlayer().getName())){
            StatsSign sign = plugin.getSignManager().getSignAt(this.signLineSettings.get(event.getPlayer().getName()));
            sign.setSignLine(event.getMessage());
            event.setCancelled(true);
            event.getPlayer().sendMessage("Sign is now ready for use!");
            this.signLineSettings.remove(event.getPlayer().getName());
        }
    }
    
    @EventHandler(ignoreCancelled=true)
    public void onBlockBreak(BlockBreakEvent event){
        Location loc = event.getBlock().getLocation();
        if(this.plugin.getSignManager().getSignAt(loc) != null){
            if(!event.getPlayer().hasPermission("stats.sign.destroy")){
                event.setCancelled(true);
                event.getPlayer().sendMessage(ChatColor.RED + "You are not allowed to destroy a stats sign!");
                return;
            }
            plugin.getSignManager().removeSign(loc);
            event.getPlayer().sendMessage(ChatColor.GREEN + "Stats sign destroyed and deleted!");
        }
    }

}
