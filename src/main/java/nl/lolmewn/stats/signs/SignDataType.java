/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.signs;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public enum SignDataType {
    
    BLOCKS_BROKEN,
    BLOCKS_PLACED,
    PLAYTIME,
    KILLS,
    DEATHS,
    PVP_KILLS,
    CUSTOM;
    
    @Override
    public String toString(){
        switch(this){
            case BLOCKS_BROKEN:
                return "blocks broken";
            case BLOCKS_PLACED:
                return "blocks placed";
            case PLAYTIME:
                return "playtime";
            case DEATHS:
                return "deaths";
            case KILLS:
                return "kills";
            case PVP_KILLS:
                return "pvpkills";
            case CUSTOM:
                return "custom";
            default:
                return null;
        }
    }
    
    public static SignDataType fromString(String input){
        if(input != null){
            for(SignDataType t : SignDataType.values()){
                if(t.toString().equalsIgnoreCase(input.toLowerCase())){
                    return t;
                }
            }
        }
        throw new IllegalArgumentException("No constant with text " + input + " found");
    }

}
