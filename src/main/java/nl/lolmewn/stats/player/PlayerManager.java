/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.player;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class PlayerManager {
    
    private Main plugin;
    private HashMap<String, StatsPlayer> players = new HashMap<String, StatsPlayer>();

    public PlayerManager(Main m) {
        plugin = m;
    }
    
    public boolean hasPlayer(String name){
        return players.containsKey(name);
    }
    
    public void addPlayer(String name, StatsPlayer player){
        this.players.put(name, player);
    }
    
    public void unloadPlayer(String name){
        this.players.remove(name);
    }
    
    public StatsPlayer getPlayer(String name){
        return this.players.get(name);
    }
    
    public void loadPlayerAsync(final String name){
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, new Runnable() {
            @Override
            public void run() {
                try {
                    Connection con = plugin.getMySQL().getConnection();
                    Statement st = con.createStatement();
                    
                    
                    
                    st.close();
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(PlayerManager.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
        });
    }

    public Iterable<StatsPlayer> getPlayers() {
        return this.players.values();
    }

}
