/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.player;

import java.util.HashMap;
import java.util.HashSet;
import nl.lolmewn.stats.StatType;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class Stat {

    private StatType type;
    private HashMap<Object[], Integer> rows = new HashMap<Object[], Integer>();
    private HashMap<Object[], Integer> currentValues = new HashMap<Object[], Integer>();
    private HashSet<Object[]> hasUpdate = new HashSet<Object[]>();
    
    public Stat(StatType type) {
        this.type = type;
    }
    
    public StatType getStatType(){
        return type;
    }
    
    public boolean hasUpdate(Object[] variables){
        return hasUpdate.contains(variables);
    }
    
    public int getUpdateValue(Object[] variables, boolean updatingDatabase){
        int update = this.hasUpdate(variables) ? this.rows.get(variables) - this.currentValues.get(variables) : this.currentValues.get(variables);
        if(updatingDatabase){
            this.currentValues.put(variables, this.rows.get(variables));
        }
        return update;
    }
    
    public void addUpdate(Object[] variables, int add){
        if(this.rows.containsKey(variables)){
            this.rows.put(variables, this.rows.get(variables) + add);
        }else{
            this.rows.put(variables, add);
        }
        if(!this.hasUpdate(variables)){
            this.hasUpdate.add(variables);
        }
    }
    
    public void setCurrentValue(Object[] variables, int value){
        this.currentValues.put(variables, value);
    }
    
    public HashSet<Object[]> getUpdateVariables(){
        return this.hasUpdate;
    }

    public int getValue(Object[] vars) {
        return rows.get(vars);
    }

}
