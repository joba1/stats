/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.player;

import java.util.EnumMap;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import nl.lolmewn.stats.StatType;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatsPlayer {
    
    private EnumMap<StatType, Stat> stats = new EnumMap<StatType, Stat>(StatType.class);
    private String playername;
    private Queue<Stat> pendingUpdates = new ConcurrentLinkedQueue<Stat>();

    public StatsPlayer(String playername) {
        this.playername = playername;
    }
    
    public String getPlayername(){
        return playername;
    }
    
    public boolean hasStat(StatType type){
        return stats.containsKey(type);
    }
    
    public void addStat(StatType type, Stat stat){
        this.stats.put(type, stat);
    }
    
    public Stat getStat(StatType type){
        return this.stats.get(type);
    }
    
    public void updateStat(StatType type, Object[] variables, int value){
        Stat stat;
        if(this.stats.containsKey(type)){
            stat = this.stats.get(type);
        }else{
            stat = new Stat(type);
        }
        stat.addUpdate(variables, value);
    }

    public Iterable<Stat> getStats() {
        return this.stats.values();
    }

}
