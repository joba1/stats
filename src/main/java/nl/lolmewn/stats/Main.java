package nl.lolmewn.stats;

import java.io.*;
import java.net.ConnectException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map.Entry;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Metrics.Graph;
import nl.lolmewn.stats.Metrics.Plotter;
import nl.lolmewn.stats.api.StatsAPI;
import nl.lolmewn.stats.player.PlayerManager;
import nl.lolmewn.stats.player.Stat;
import nl.lolmewn.stats.player.StatsPlayer;
import nl.lolmewn.stats.signs.SignCommands;
import nl.lolmewn.stats.signs.SignDataGetter;
import nl.lolmewn.stats.signs.SignListener;
import nl.lolmewn.stats.signs.SignManager;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.craftbukkit.libs.com.google.gson.Gson;
import org.bukkit.craftbukkit.libs.com.google.gson.GsonBuilder;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.ServicePriority;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener {

    private Settings settings;
    public boolean newConfig;
    private boolean query = false;
    private MySQL mysql;
    private StatsAPI api;
    private SignManager signManager;
    private SignDataGetter signDataGetter;
    private PlayerManager playerManager;
    protected List<Runnable> waitings = new ArrayList<Runnable>();
    private Map<String, MoveHolder> moveHolders = new ConcurrentHashMap<String, MoveHolder>();
    private Queue<String> playTimeQueue = new ConcurrentLinkedQueue<String>();
    private Queue<QueryHolder> queries = new ConcurrentLinkedQueue<QueryHolder>();
    protected double moveTaken = 0;
    private String server = "stats.lolmewn.nl"; //default
    private int unableToConnectToGlobal = 0;
    protected boolean beingConfigged = false;
    protected Queue<QueryHolder> globalQueue = new ConcurrentLinkedQueue<QueryHolder>();
    private int queriesExecuted = 0;

    @Override
    public void onDisable() {
        this.signManager.save();
        if (this.mysql != null && !this.mysql.isFault()) {
            this.runTableUpdates();
            this.mysql.exit();
        }
        if (this.canSendToGlobal()) {
            this.sendStats(server);
        }
        this.getServer().getScheduler().cancelTasks(this);
    }

    @Override
    public void onEnable() {
        this.settings = new Settings(this);
        this.newConfig = this.setupConfig();
        this.checkNewSettings();
        this.settings.loadSettings();
        if (this.getServer().getPluginManager().getPlugin("Votifier") != null) {
            this.getServer().getPluginManager().registerEvents(new VotifierListener(this), this);
        }
        if (!newConfig) {
            this.mysql = new MySQL(this, this.getSettings().getDbHost(), this.getSettings().getDbPort(),
                    this.getSettings().getDbUser(), this.getSettings().getDbPass(),
                    this.getSettings().getDbName(), this.getSettings().getDbPrefix());
            if (this.mysql.isFault()) {
                this.getLogger().severe("MySQL connection failed, disabling plugin!");
                this.getServer().getPluginManager().disablePlugin(this);
                return;
            }
        }
        this.getServer().getPluginManager().registerEvents(new EventListener(this), this);
        //Schedule Play time rec every second.
        this.getServer().getScheduler().scheduleSyncRepeatingTask(this, new Runnable() {

            @Override
            public void run() {
                for (Player p : getServer().getOnlinePlayers()) {
                    if (!p.hasPermission("stats.track")) {
                        continue;
                    }
                    playTimeQueue.add(p.getName());
                }
            }
        }, 20L, 20L);

        //Schedule the updating of all tables and some debug
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {

            @Override
            public void run() {
                if (!newConfig) {
                    runTableUpdates();
                }
                if (moveTaken != 0) {
                    debug("Move Event taken: " + moveTaken + " ms");
                    moveTaken = 0;
                }
            }
        }, 200L, 200L);
        //Enable (or not) Sending of data to global server
        if (this.getSettings().isSendToGlobal()) {
            if (this.canSendToGlobal()) {
                this.debug("Setting up globalserver sending every 600 ticks.");
                this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {

                    @Override
                    public void run() {
                        String re = sendStats(server);
                        if (!re.equals("")) {
                            debug(re);
                        }
                    }
                }, 700, 600);
            } else {
                this.getLogger().warning("Not sending to global server due to online-mode=false");
                this.getLogger().warning("To fully use all of this plugins capabilities, please use online-mode=true");
            }
        }
        if (this.newConfig) {
            this.getServer().getPluginManager().registerEvents(new Listener() {

                @EventHandler
                public void join(PlayerJoinEvent e) {
                    if (!newConfig) {
                        return;
                    }
                    if (e.getPlayer().hasPermission("stats.config") || e.getPlayer().isOp() && !beingConfigged) {
                        beingConfigged = true;
                        startConfigurator(e.getPlayer());
                    }
                }
            }, this);
        }
        if (this.getSettings().isUpdate()) {
            new Updater(this, "lolmewnstats", this.getFile(), Updater.UpdateType.DEFAULT, true);
        }
        try {
            Metrics m = new Metrics(this);
            Graph g = m.createGraph("Sending Data to Global Server");
            g.addPlotter(new Plotter() {

                @Override
                public String getColumnName() {
                    return "Enabled in config";
                }

                @Override
                public int getValue() {
                    return getSettings().isSendToGlobal() ? 1 : 0;
                }
            });
            g.addPlotter(new Plotter() {

                @Override
                public String getColumnName() {
                    return "Allowed to send";
                }

                @Override
                public int getValue() {
                    return canSendToGlobal() ? 1 : 0;
                }
            });
            Graph d = m.createGraph("Queries executed");
            d.addPlotter(new Plotter() {

                @Override
                public int getValue() {
                    if (mysql == null) {
                        return 0;
                    }
                    int done = queriesExecuted;
                    queriesExecuted = 0;
                    return done;
                }
            });
            m.addGraph(g);
            m.addGraph(d);
            m.start();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        registerAPI();
        this.signManager = new SignManager(this);
        this.signManager.load();
        this.signDataGetter = new SignDataGetter(this);
        this.playerManager = new PlayerManager(this);
        this.getServer().getPluginManager().registerEvents(new SignListener(this), this);
        this.getServer().getScheduler().runTaskTimerAsynchronously(this, signDataGetter, 200, 200);
        this.getLogger().info("Version " + this.getDescription().getVersion() + " enabled!");
    }

    protected String getBlockTable() {
        return this.getSettings().getDbPrefix() + "block";
    }

    protected String getMoveTable() {
        return this.getSettings().getDbPrefix() + "move";
    }

    protected String getKillTable() {
        return this.getSettings().getDbPrefix() + "kill";
    }

    protected String getDeathTable() {
        return this.getSettings().getDbPrefix() + "death";
    }

    protected String getPlayerTable() {
        return this.getSettings().getDbPrefix() + "player";
    }

    public synchronized MySQL getMySQL() {
        return this.mysql;
    }

    public SignManager getSignManager() {
        return this.signManager;
    }

    public Settings getSettings() {
        return this.settings;
    }

    public Map<String, MoveHolder> getMoveHolders() {
        return this.moveHolders;
    }

    public PlayerManager getPlayerManager() {
        return this.playerManager;
    }

    protected void addQueryToQueue(String sql, String insert, Object... vars) {
        this.queries.add(new QueryHolder(sql, insert.split("ON")[0], vars));
    }

    private boolean setupConfig() {
        if (!new File(this.getDataFolder(), "config.yml").exists()) {
            this.saveResource("config.yml", false);
            return true;
        }
        return false;
    }

    private void checkNewSettings() {
        if (!getConfig().getBoolean("ignoreCreative")) {
            this.debug("Adding ignoreCreative to config");
            getConfig().set("ignoreCreative", false);
        }
        saveConfig();
    }

    public void debug(String message) {
        if (this.getSettings().isDebugging()) {
            this.getLogger().info("[Debug] " + message);
        }
    }

    public void debugQuery(String message) {
        if (this.query) {
            this.getLogger().info("[Debug][Q] " + message);
        }
    }

    public int executeStatement(String statement) {
        try {
            Connection con = this.getMySQL().getConnection();
            Statement st = con.createStatement();
            int re = st.executeUpdate(statement);
            st.close();
            con.close();
            return re;
        } catch (SQLException e) {
            if (!e.getClass().getSimpleName().equals("MysqlDataTruncation")) {
                this.getLogger().warning(e.toString());
                this.getLogger().warning("Attempted query, failed with exception above. Query: " + statement);
            } else {
                this.debug("MySQLDataTruncation happened! Ah well :)");
            }
        }
        return -1;
    }

    public void addBlockToQueue(String player, int id, byte data, boolean breaking) {
        this.addQueryToQueue("UPDATE " + this.getBlockTable() + " SET amount=amount+? WHERE player=? AND blockID=? AND blockData=? AND break=?", "INSERT INTO " + this.getBlockTable() + ""
                + "(amount, player, blockID, blockData, break)"
                + "VALUES (?,?,?,?,?)", 1, player, id, data, breaking);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String arg, String[] args) {
        //cmd is /stats
        if (args.length == 0) {
            if (!(sender instanceof Player)) {
                sender.sendMessage("Console cannot perform this command.");
                return true;
            }
            sendSomeCoolStats(sender, sender.getName());
            return true;
        }
        if (args[0].equals("toggle")) {
            if (!sender.hasPermission("stats.toggle")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            if (args.length == 1) {
                sender.sendMessage("USAGE: /stats toggle <debug|query>");
                return true;
            }
            if (args[1].equals("debug")) {
                this.getSettings().setDebugging(!this.getSettings().isDebugging());
                sender.sendMessage("Debug value set to: " + this.getSettings().isDebugging());
                return true;
            }
            if (args[1].equals("query")) {
                this.query = !this.query;
                sender.sendMessage("Query debug value set to: " + this.query);
                return true;
            }
        }
        if (args[0].equalsIgnoreCase("sign")) {
            return new SignCommands().onCommand(this, sender, args);
        }
        if (args[0].equals("sendStats")) {
            if (!sender.hasPermission("stats.sendGlobal")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            sender.sendMessage(this.sendStats(server));
            return true;
        }
        if (args[0].equals("reload")) {
            if (!sender.hasPermission("stats.reload")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            //send all stats that are queued
            sender.sendMessage(ChatColor.BLUE + "Sending all remaining stats...");
            this.runTableUpdates();
            if (this.canSendToGlobal()) {
                sender.sendMessage(ChatColor.BLUE + "Sending remaining data to global server...");
                sender.sendMessage(ChatColor.GREEN + this.sendStats(server));
            }
            sender.sendMessage(ChatColor.BLUE + "Reloading plugin...");
            Settings set = new Settings(this);
            set.loadSettings();
            this.settings = set;
            this.mysql = new MySQL(this, set.getDbHost(), set.getDbPort(),
                    set.getDbUser(), set.getDbPass(),
                    set.getDbName(), set.getDbPrefix());
            if (this.mysql.isFault()) {
                this.getLogger().severe("MySQL connection failed, disabling plugin!");
                sender.sendMessage(ChatColor.RED + "MySQL connection failed, disabling plugin!");
                this.getServer().getPluginManager().disablePlugin(this);
                return true;
            }
            sender.sendMessage(ChatColor.GREEN + "Plugin reloaded succesfully!");
            return true;
        }
        if (args[0].equalsIgnoreCase("debug")) {
            if (!sender.hasPermission("stats.debug")) {
                sender.sendMessage("You do not have permissions to do this!");
                return true;
            }
            if (args.length == 1) {
                sender.sendMessage("Correct usage: /stats debug global");
                return true;
            }
            this.server = args[1];
            sender.sendMessage("Global server set to " + server);
            return true;
        }
        if (sender.hasPermission("stats.view.others")) {
            Player find = this.getServer().getPlayer(args[0]);
            if (find == null) {
                OfflinePlayer ofpl = this.getServer().getOfflinePlayer(args[0]);
                if (ofpl == null) {
                    sender.sendMessage("Player " + args[0] + " not found!");
                    return true;
                }
                this.sendSomeCoolStats(sender, ofpl.getName());
                return true;
            }
            this.sendSomeCoolStats(sender, find.getName());
            return true;
        } else {
            sender.sendMessage(ChatColor.RED + "Sorry, you do not have permissions to view someone elses stats!");
            return true;
        }
    }
    private boolean sendingStatsGlobal = false;

    private String sendStats(String server) {
        if (this.sendingStatsGlobal) {
            return "Stats already sending, cancelling.";
        }
        Socket soc = null;
        try {
            this.debug("Connecting to global server...");
            soc = new Socket(server, 1888);
            this.sendingStatsGlobal = true;
            this.debug("Connected to global server.");
            PrintWriter out = new PrintWriter(soc.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(soc.getInputStream()));
            out.println("CPV3");
            if (!in.readLine().equalsIgnoreCase("ready for servername")) {
                debug("Global server didn't want servername :'(");
            }
            out.println(this.getServer().getServerName());
            if (!in.readLine().equalsIgnoreCase("ready for serverport")) {
                debug("Global server didn't want serverport :'(");
            }
            out.println(this.getServer().getPort());
            if (this.globalQueue.isEmpty()) {
                out.println("ping");
                in.close();
                out.flush();
                out.close();
                soc.close();
                this.sendingStatsGlobal = false;
                return "Just pinged.";
            } else {
                while (!this.globalQueue.isEmpty()) {
                    QueryHolder h = this.globalQueue.poll();
                    h.makeReadyForGlobal(this.getSettings().getDbPrefix());
                    Gson g = new GsonBuilder().setDateFormat("yyy-MM-dd hh:mm:ss.S").create();
                    String send = g.toJson(h);
                    out.println(send);
                }
                out.println("end");
                String read = in.readLine();
                this.unableToConnectToGlobal = 0;
                in.close();
                out.flush();
                out.close();
                this.sendingStatsGlobal = false;
                return read;
            }

        } catch (UnknownHostException ex) {
            this.getLogger().warning("Couldn't connect to global server! Your DNS lookup might be broken or inactive!");
        } catch (ConnectException ex) {
            if (this.unableToConnectToGlobal == 0 || this.unableToConnectToGlobal % 60 == 0) {
                this.getLogger().warning("Couldn't connect to global server! Maybe it's offline...");
            }
            this.unableToConnectToGlobal++;
        } catch (IOException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        } catch (NullPointerException ex){
            if (this.unableToConnectToGlobal == 0 || this.unableToConnectToGlobal % 60 == 0) {
                this.getLogger().warning("Couldn't connect to global server! Maybe it's offline...");
            }
            this.unableToConnectToGlobal++;
        } 
        finally {
            this.sendingStatsGlobal = false;
            try {
                if (soc != null) {
                    soc.close();
                }
            } catch (IOException ex) {
            }
        }
        return "Something failed while trying to send stats to global server!";
    }
    private boolean working;

    private void runTableUpdates() {
        if (working) {
            debug("stats were still sending, postphoning.");
            return;
        }
        working = true;
        long start = System.nanoTime();
        Connection con = getMySQL().getConnection();
        this.runMoveTableUpdate();
        this.runPlayTimeTableUpdate();
        try {
            this.runOtherUpdates(con);
        } catch (Exception e) {
            System.out.println("Exception happened: " + e.getMessage());
            e.printStackTrace();
        }
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        long taken = (System.nanoTime() - start) / 1000000;
        if (taken != 0) {
            this.debug("Time taken sending table updates: " + taken);
        }
        working = false;
    }

    public void runMoveTableUpdate() {
        Iterator<Entry<String, MoveHolder>> it = this.getMoveHolders().entrySet().iterator();
        while (it.hasNext()) {
            Entry<String, MoveHolder> entry = it.next();
            for (int type : entry.getValue().getDistances().keySet()) {
                this.addQueryToQueue("UPDATE " + getMoveTable() + " SET distance=distance+? WHERE player=? AND type=?",
                        "INSERT INTO " + getMoveTable()
                        + " (distance, player, type) VALUES (?,?,?)",
                        entry.getValue().getDistance(type), entry.getKey(), type);
            }
            it.remove();
        }
    }

    private void runPlayTimeTableUpdate() {
        HashMap<String, Integer> p = new HashMap<String, Integer>();
        while (!this.playTimeQueue.isEmpty()) {
            String player = this.playTimeQueue.poll();
            if (p.containsKey(player)) {
                p.put(player, p.get(player) + 1);
            } else {
                p.put(player, 1);
            }
        }
        for (String player : p.keySet()) {
            this.addQueryToQueue("UPDATE " + this.getPlayerTable() + " SET playtime=playtime+? WHERE player=?",
                    "INSERT INTO " + this.getPlayerTable() + " (playtime, player) VALUES (?,?)", p.get(player), player);
        }
    }

    private void runOtherUpdates(Connection con) {
        if (this.queries.isEmpty()) {
            return;
        }
        this.executeQueue(con);
        if (!this.canSendToGlobal()) {
            this.globalQueue.clear();
        }
    }

    public void executeQueue(Connection con) {
        if (queries.isEmpty()) {
            return;
        }
        List<QueryHolder> process = new ArrayList<QueryHolder>();
        while (!queries.isEmpty()) {
            for (int i = 0; i < 1000; i++) {
                QueryHolder q = queries.poll();
                if (q == null) {
                    break;
                }
                process.add(q);
            }
        }
        this.globalQueue.addAll(process);
        Map<String, List<VarHolder>> qs = this.getSameSQL(process);
        try {
            con.setAutoCommit(false);
            PreparedStatement st;
            for (String sql : qs.keySet()) {
                List<VarHolder> original = qs.get(sql);
                List<VarHolder> values = this.removeDuplicates(original);
                if (sql.contains("$$prefix")) {
                    //thats fucked up weird, lets replace that.
                    sql = sql.replace("$$prefix", this.getSettings().getDbPrefix());
                }
                st = con.prepareCall(sql);
                for (Iterator<VarHolder> it = values.iterator(); it.hasNext();) {
                    VarHolder vars = it.next();
                    for (int position = 0; position < vars.vars.length; position++) {
                        st.setObject(position + 1, vars.vars[position]);
                    }
                    st.addBatch();
                }
                int[] updated = st.executeBatch();
                PreparedStatement insertPS = null;
                for (int i = 0; i < updated.length; i++) {
                    if (updated[i] == 0) {
                        String insertQuery = values.get(i).insert;
                        if (insertQuery == null) {
                            continue;
                        }
                        insertPS = con.prepareCall(insertQuery);
                        for (int position = 0; position < values.get(i).vars.length; position++) {
                            insertPS.setObject(position + 1, values.get(i).vars[position]);
                        }
                        insertPS.execute();
                    }
                }
                con.commit();
                st.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        if (!this.queries.isEmpty()) {
            this.executeQueue(con); //another 1000!
        }
        this.executeNewQueue(con);
    }

    private void executeNewQueue(Connection con) {
        try {
            con.setAutoCommit(false);
            for (StatsPlayer player : this.getPlayerManager().getPlayers()) {
                for (Stat stat : player.getStats()) {

                    PreparedStatement st = con.prepareStatement(stat.getStatType().getUpdateStatement(this.getSettings().getDbPrefix()));
                    List<Object[]> updateVars = new ArrayList<Object[]>(stat.getUpdateVariables());
                    for (Object[] updateVar : updateVars) {
                        st.setObject(1, stat.getUpdateValue(updateVar, true));
                        for (int i = 0; i < updateVar.length; i++) {
                            Object o = updateVar[i];
                            st.setObject(i + 2, o);
                        }
                        st.addBatch();
                    }
                    st.close();
                    int[] updated = st.executeBatch();
                    for (int i = 0; i < updated.length; i++) {
                        st = con.prepareStatement(stat.getStatType().getInsertStatement(this.getSettings().getDbPrefix()));
                        Object[] vars = updateVars.get(i);
                        for (int j = 0; j < vars.length; j++) {
                            st.setObject(j + 1, vars[j]);
                        }
                        st.setObject(vars.length + 1, stat.getValue(vars));
                        st.executeUpdate();
                        st.close();
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    protected Map<String, List<VarHolder>> getSameSQL(List<QueryHolder> list) {
        Map<String, List<VarHolder>> map = new HashMap<String, List<VarHolder>>();
        for (QueryHolder q : list) {
            if (map.containsKey(q.getUpdate())) {
                map.get(q.getUpdate()).add(new VarHolder(q.getVars(), q.getInsert()));
            } else {
                List<VarHolder> oList = new ArrayList<VarHolder>();
                oList.add(new VarHolder(q.getVars(), q.getInsert()));
                map.put(q.getUpdate(), oList);
            }
        }

        return map;
    }

    public List<VarHolder> removeDuplicates(List<VarHolder> original) {
        List<VarHolder> back = new ArrayList<VarHolder>();
        HashMap<String, VarHolder> map = new HashMap<String, VarHolder>();
        for (VarHolder var : original) {
            if ((!(var.vars[0] instanceof Integer)) && (!(var.vars[0] instanceof Double))) {
                back.add(var);
                continue;
            }
            Object[] allButOne = new Object[var.vars.length];
            for (int i = 1; i < var.vars.length; i++) {
                allButOne[i - 1] = var.vars[i];
            }
            if (map.containsKey(Arrays.toString(allButOne))) {
                map.get(Arrays.toString(allButOne)).updateValue += var.vars[0] instanceof Integer ? (Integer) var.vars[0] : ((Double) var.vars[0]).doubleValue();
            } else {
                map.put(Arrays.toString(allButOne), var.addToUpdate((var.vars[0] instanceof Integer ? ((Integer) var.vars[0]) : ((Double) var.vars[0]).doubleValue())));

            }
        }
        for (Iterator<String> it = map.keySet().iterator(); it.hasNext();) {
            String hash = it.next();
            VarHolder varHolder = map.get(hash);
            varHolder.vars[0] = varHolder.updateValue;
            if (varHolder.insert != null && varHolder.insert.contains("block")) {
                //block table
                varHolder.vars[3] = ((Byte) varHolder.vars[3]).byteValue();
            }
            back.add(varHolder);
        }

        return back;
    }

    protected boolean setupMySQL(ConversationContext cc) {
        this.mysql = new MySQL(this,
                (String) cc.getSessionData("MySQL-Host"),
                Integer.parseInt((String) cc.getSessionData("MySQL-Port")),
                (String) cc.getSessionData("MySQL-User"),
                (String) cc.getSessionData("MySQL-Pass"),
                (String) cc.getSessionData("MySQL-Database"),
                "Stats_");
        return !this.mysql.isFault();
    }

    protected void startConfigurator(Player p) {
        new Configurator(this, p);
    }

    protected void saveValues(ConversationContext cc) {
        getConfig().set("MySQL-Host", (String) cc.getSessionData("MySQL-Host"));
        getConfig().set("MySQL-Port", cc.getSessionData("MySQL-Port"));
        getConfig().set("MySQL-User", (String) cc.getSessionData("MySQL-User"));
        getConfig().set("MySQL-Pass", (String) cc.getSessionData("MySQL-Pass"));
        getConfig().set("MySQL-Database", (String) cc.getSessionData("MySQL-Database"));
        saveConfig();
    }

    private void sendSomeCoolStats(CommandSender sender, String from) {
        final String name = sender.getName();
        final String lookup = from;
        this.getServer().getScheduler().runTaskAsynchronously(this, new Runnable() {

            @Override
            public void run() {
                try {
                    int playTimeSeconds = api.getPlaytime(lookup);
                    final String playtime = String.format("%d days, %02d hours, %02d mins, %02d secs",
                            TimeUnit.SECONDS.toDays(playTimeSeconds),
                            TimeUnit.SECONDS.toHours(playTimeSeconds) - TimeUnit.SECONDS.toDays(playTimeSeconds) * 24,
                            TimeUnit.SECONDS.toMinutes(playTimeSeconds) - TimeUnit.SECONDS.toHours(playTimeSeconds) * 60,
                            TimeUnit.SECONDS.toSeconds(playTimeSeconds) - TimeUnit.SECONDS.toMinutes(playTimeSeconds) * 60);
                    final int blocksbroken = api.getTotalBlocksBroken(lookup);
                    final int blocksplaced = api.getTotalBlocksPlaced(lookup);
                    getServer().getScheduler().scheduleSyncDelayedTask(Bukkit.getPluginManager().getPlugin("Stats"), new Runnable() {

                        @Override
                        public void run() {
                            Player sender = getServer().getPlayer(name);
                            if (sender == null) {
                                return;
                            }
                            sender.sendMessage(ChatColor.LIGHT_PURPLE + "Time played: " + ChatColor.GREEN + playtime);
                            sender.sendMessage(ChatColor.LIGHT_PURPLE + "Total blocks broken: " + ChatColor.GREEN + blocksbroken);
                            sender.sendMessage(ChatColor.LIGHT_PURPLE + "Total blocks placed: " + ChatColor.GREEN + blocksplaced);
                        }
                    });
                } catch (SQLException ex) {
                    Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });

    }

    private void registerAPI() {
        this.api = new StatsAPI(this);
        this.getServer().getServicesManager().register(StatsAPI.class, api, this, ServicePriority.Low);
    }

    protected boolean canSendToGlobal() {
        return this.getSettings().isSendToGlobal() && (this.getServer().getOnlineMode() || this.getServer().getIp().equals("127.0.0.1") || this.getServer().getIp().equals("localhost"));
    }

    public void configComplete() {
        for (Runnable r : this.waitings) {
            this.getServer().getScheduler().runTaskAsynchronously(this, r);
        }
    }
}
