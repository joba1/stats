package nl.lolmewn.stats.converter;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.Main;

/**
 * @author Lolmewn
 */
public class BeardStats implements Converter{
    
    private Main m;
    
    public BeardStats(Main main){
        this.m = main;
    }

    @Override
    public void execute() {
        try {
            Connection con = m.getMySQL().getConnection();
            Statement st = con.createStatement();
            ResultSet set = st.executeQuery("SELECT * FROM stats");
            if(set == null){
                return;
            }
            while(set.next()){
                String player = set.getString("player");
            }
            
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(BeardStats.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
