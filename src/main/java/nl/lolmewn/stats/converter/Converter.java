package nl.lolmewn.stats.converter;

/**
 * @author Lolmewn
 */
public interface Converter {
    
    public void execute();

}
