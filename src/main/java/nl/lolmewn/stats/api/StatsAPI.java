/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import nl.lolmewn.stats.Main;
import nl.lolmewn.stats.signs.SignManager;
import org.bukkit.Location;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class StatsAPI {

    private Main plugin;
    
    public StatsAPI(Main plugin) {
        this.plugin = plugin;
    }
    
    public int getPlaytime(String player) throws SQLException{
        Connection con = plugin.getMySQL().getConnection();
        con.setAutoCommit(true);
        PreparedStatement st = con.prepareStatement("SELECT playtime FROM " + plugin.getSettings().getDbPrefix() + "player WHERE player=? LIMIT 1");
        st.setString(1, player);
        ResultSet set = st.executeQuery();
        if(!set.next()){
            return 0;
        }
        int playtime = set.getInt("playtime");
        set.close();
        st.close();
        con.close();
        return playtime;
    }
    
    public int getTotalBlocksBroken(String player) throws SQLException{
        return this.getTotalBlocks(player, true);
    }
    
    public int getTotalBlocksPlaced(String player) throws SQLException{
        return this.getTotalBlocks(player, false);
    }
    
    public int getTotalBlocks(String player, boolean broken) throws SQLException{
        Connection con = plugin.getMySQL().getConnection();
        con.setAutoCommit(true);
        PreparedStatement st = con.prepareStatement("SELECT SUM(amount) AS total FROM " + plugin.getSettings().getDbPrefix() + "block WHERE player=? AND break=?");
        st.setString(1, player);
        st.setBoolean(2, broken);
        ResultSet set = st.executeQuery();
        if(!set.next()){
            return 0;
        }
        int total = set.getInt("total");
        set.close();
        st.close();
        con.close();
        return total;
    }
    
    @Deprecated
    public ResultSet queryDatabase(String sql) throws SQLException{
        return this.plugin.getMySQL().getConnection().createStatement().executeQuery(sql);
    }
    
    public String getDatabasePrefix(){
        return this.plugin.getSettings().getDbPrefix();
    }
    
    public SignManager getSignManager(){
        return plugin.getSignManager();
    }
    
    /**
     * Retrieves and returns a connection from the pool. Don't forget to close it afterwards!
     * @return Connection from the pool
     */
    public Connection getConnection(){
        return plugin.getMySQL().getConnection();
    }
    
    public boolean isStatsSign(Location loc){
        return plugin.getSignManager().getSignAt(loc) != null;
    }

}
