package nl.lolmewn.stats;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import nl.lolmewn.stats.mysql.MySQLLib;

public class MySQL {

    private String prefix; //host, username, password, database, prefix;
    //private int port;
    private boolean fault;
    private Main plugin;
    
    private MySQLLib mysql;

    public MySQL(Main main, String host, int port, String username, String password, String database, String prefix) {
        this.plugin = main;
        this.prefix = prefix;
        this.mysql = new MySQLLib(main.getLogger(), prefix, host, Integer.toString(port), database, username, password);
        Connection con;
        if((con = this.mysql.open()) != null){
            try {
                con.close();
            } catch (SQLException ex) {
                Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
            }
            this.setupTables();
            this.checkColumns();
            this.checkIndexes();
        }else{
            this.setFault(true);
        }
    }

    private void setupTables() {
        if (this.isFault()) {
            return;
        }
        this.executeStatement("CREATE TABLE IF NOT EXISTS " + this.prefix + "block"
                + "(counter int PRIMARY KEY NOT NULL AUTO_INCREMENT, "
                + "player varchar(255) NOT NULL, "
                + "blockID int NOT NULL, "
                + "blockData BLOB NOT NULL, "
                + "amount int NOT NULL, break boolean NOT NULL)");
        this.executeStatement("CREATE TABLE IF NOT EXISTS " + this.prefix + "move"
                + "(counter int PRIMARY KEY NOT NULL AUTO_INCREMENT, "
                + "player varchar(255) NOT NULL, "
                + "type tinyint NOT NULL," //type 0 = walk, 1=boat, 2=minecraft, 3=pig
                + "distance double NOT NULL)");
        this.executeStatement("CREATE TABLE IF NOT EXISTS " + this.prefix + "kill"
                + "(counter int PRIMARY KEY NOT NULL AUTO_INCREMENT, "
                + "player varchar(255) NOT NULL, "
                + "type varchar(32) NOT NULL , "
                + "amount int NOT NULL)");
        this.executeStatement("CREATE TABLE IF NOT EXISTS " + this.prefix + "death"
                + "(counter int PRIMARY KEY NOT NULL AUTO_INCREMENT, "
                + "player varchar(255) NOT NULL, "
                + "cause varchar(32) NOT NULL , "
                + "amount int NOT NULL,"
                + "entity boolean NOT NULL)");
        this.executeStatement("CREATE TABLE IF NOT EXISTS " + this.prefix + "player"
                + "(counter int PRIMARY KEY NOT NULL AUTO_INCREMENT, "
                + "player varchar(255) NOT NULL, "
                + "playtime int NOT NULL DEFAULT 0, "
                + "arrows int DEFAULT 0,"
                + "xpgained int DEFAULT 0,"
                + "joins int DEFAULT 0,"
                + "fishcatch int DEFAULT 0,"
                + "damagetaken int DEFAULT 0,"
                + "timeskicked int DEFAULT 0,"
                + "toolsbroken int DEFAULT 0,"
                + "eggsthrown int DEFAULT 0,"
                + "itemscrafted int DEFAULT 0,"
                + "omnomnom int DEFAULT 0,"
                + "onfire int DEFAULT 0,"
                + "wordssaid int DEFAULT 0,"
                + "commandsdone int DEFAULT 0,"
                + "votes int DEFAULT 0,"
                + "lastjoin TIMESTAMP DEFAULT NOW(),"
                + "lastleave TIMESTAMP DEFAULT 0)");
    }

    public boolean isFault() {
        return fault;
    }

    private void setFault(boolean fault) {
        this.fault = fault;
    }

    public int executeStatement(String statement) {
        if (isFault()) {
            System.out.println("[Stats] Can't execute statement, something wrong with connection");
            return 0;
        }
        this.plugin.debugQuery("Executing Statement: " + statement);
        try {
            Connection con = this.mysql.getConnection();
            con.setAutoCommit(true);
            Statement state = con.createStatement();
            int re = state.executeUpdate(statement);
            state.close();
            con.close();
            return re;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }
    
    private void checkColumns(){
        Connection con = this.getConnection();
        this.checkColumn(con, this.prefix + "death", "entity", "boolean", null);
        this.checkColumn(con, this.prefix + "player", "lastjoin", "TIMESTAMP", "0");
        this.checkColumn(con, this.prefix + "player", "lastleave", "TIMESTAMP", "0");
        this.checkColumn(con, this.prefix + "player", "votes", "int", "0");
        this.executeStatement("ALTER TABLE " + this.prefix + "block CHANGE COLUMN blockData blockData BLOB NOT NULL");
        try {
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void checkColumn(Connection con, String table, String column, String type, String def){
        this.plugin.debug("Checking Column for " + table + ", " + column + " as " + type);
        try {
            DatabaseMetaData md = con.getMetaData();
            ResultSet rs = md.getColumns(null, null, table, column);
            if (rs.next()) {
                this.plugin.debug(column + " was already in the table!");
                return;
            }
            this.executeStatement("ALTER TABLE " + table + " ADD COLUMN " + column + " " + type + (def == null ? "" : " DEFAULT " + def));
            rs.close();
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public Connection getConnection(){
        return this.mysql.getConnection();

    }
    
    private void checkIndexes() {
        try {
            Connection con = this.mysql.getConnection();
            con.setAutoCommit(true);
            Statement st = con.createStatement();
            ResultSet set;
            if((set = st.executeQuery("SELECT ENGINE FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA=DATABASE() AND TABLE_NAME LIKE '" + this.prefix + "%'")).next()){
                if(set.getString("ENGINE").equalsIgnoreCase("InnoDB")){
                    set.close();
                    st.execute("SET SESSION old_alter_table=1");
                }
            }
             //for InnoDB
            if(!st.executeQuery("SHOW INDEXES FROM " + prefix + "block WHERE Key_name='no_duplicates'").next()){
                st.execute("ALTER IGNORE TABLE " + prefix + "block ADD UNIQUE INDEX no_duplicates (player, blockID, blockData(4), break)");
            }
            if(!st.executeQuery("SHOW INDEXES FROM " + prefix + "player WHERE Key_name='no_duplicates'").next()){
                st.execute("ALTER IGNORE TABLE " + prefix + "player ADD UNIQUE INDEX no_duplicates (player)");
            }
            if(!st.executeQuery("SHOW INDEXES FROM " + prefix + "move WHERE Key_name='no_duplicates'").next()){
                st.execute("ALTER IGNORE TABLE " + prefix + "move ADD UNIQUE INDEX no_duplicates (player, type)");
            }
            if(!st.executeQuery("SHOW INDEXES FROM " + prefix + "kill WHERE Key_name='no_duplicates'").next()){
                st.execute("ALTER IGNORE TABLE " + prefix + "kill ADD UNIQUE INDEX no_duplicates (player, type)");
            }
            if(!st.executeQuery("SHOW INDEXES FROM " + prefix + "death WHERE Key_name='no_duplicates'").next()){
                st.execute("ALTER IGNORE TABLE " + prefix + "death ADD UNIQUE INDEX no_duplicates (player, cause, entity)");
            }
            st.close();
            con.close();
        } catch (SQLException ex) {
            Logger.getLogger(MySQL.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    void exit() {
        this.mysql.exit();
    }

}