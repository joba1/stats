/*
 *  Copyright 2013 Lolmewn <info@lolmewn.nl>.
 */

package nl.lolmewn.stats;

import com.vexsoftware.votifier.model.VotifierEvent;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class VotifierListener implements Listener{

    private Main plugin;
    
    public VotifierListener(Main plugin) {
        this.plugin = plugin;
    }
    
    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void vote(VotifierEvent event){
        this.plugin.addQueryToQueue("UPDATE " + plugin.getPlayerTable() + 
                " SET votes=votes+? WHERE player=?", "INSERT INTO " + plugin.getPlayerTable()
                + "(votes, player) VALUES (?,?)", 1, event.getVote().getUsername());
    }

}
