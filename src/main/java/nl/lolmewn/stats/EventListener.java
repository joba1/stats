package nl.lolmewn.stats;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.bukkit.GameMode;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.CraftItemEvent;
import org.bukkit.event.player.*;

/**
 *
 * @author Lolmewn <info@lolmewn.nl>
 */
public class EventListener implements Listener {

    private final Main plugin;
    private String permissionNode = "stats.track";

    private Main getPlugin() {
        return this.plugin;
    }

    public EventListener(Main aThis) {
        this.plugin = aThis;
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockBreak(BlockBreakEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (event.getBlock().getTypeId() == 0) {
            return;
        }
        this.plugin.addBlockToQueue(event.getPlayer().getName(), event.getBlock().getTypeId(), event.getBlock().getData(), true);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onBlockPlace(BlockPlaceEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        this.plugin.addBlockToQueue(event.getPlayer().getName(), event.getBlock().getTypeId(), event.getBlock().getData(), false);
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onPlayerMove(final PlayerMoveEvent event) {
        if (event instanceof PlayerTeleportEvent) {
            return;
        }
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (!event.getFrom().getWorld().equals(event.getTo().getWorld())) {
            return;
        }
        Player p = event.getPlayer();
        double distance = event.getFrom().distance(event.getTo());
        if (distance < 0.0001 || distance > 10) {
            return;
        }
        int type = 0;
        if (p.isInsideVehicle()) {
            Entity vehicle = p.getVehicle();
            if (vehicle instanceof Boat) {
                type = 1;
            }
            if (vehicle instanceof Minecart) {
                type = 2;
            }
            if (vehicle instanceof Pig) {
                if(vehicle.isInsideVehicle() && vehicle.getVehicle() instanceof Minecart){
                    type = 4;
                }else{
                    type = 3;
                }
            }
        }
        MoveHolder h = this.getPlugin().getMoveHolders().get(p.getName());
        if (h == null) {
            h = new MoveHolder();
            h.addDistance(distance, type);
            getPlugin().getMoveHolders().put(p.getName(), h);
        } else {
            h.addDistance(distance, type);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onEntityDeath(EntityDeathEvent event) {
        EntityType e = event.getEntityType();
        if (event.getEntity().getKiller() != null) {
            if (!event.getEntity().getKiller().hasPermission(permissionNode)) {
                return;
            }
            if (this.getPlugin().getSettings().isIgnoreCreative() && event.getEntity().getKiller().getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }
            this.getPlugin().addQueryToQueue("UPDATE " + getPlugin().getKillTable() + " SET amount=amount+? WHERE player=? AND type=?",
                    "INSERT INTO " + getPlugin().getKillTable()
                    + "(amount, player, type) VALUES "
                    + "(?,?,?)",
                    1, event.getEntity().getKiller().getName(), e.toString().substring(0, 1) + e.toString().substring(1).toLowerCase());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerDeath(PlayerDeathEvent event) {
        if (!event.getEntity().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getEntity().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        EntityDamageEvent e = event.getEntity().getLastDamageCause();
        Player dead = event.getEntity();
        if (this.getPlugin().getSettings().isIgnoreCreative() && dead.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (e instanceof EntityDamageByEntityEvent) {
            EntityDamageByEntityEvent ev = (EntityDamageByEntityEvent) e;
            if (ev.getDamager() instanceof Arrow) {
                Arrow a = (Arrow) ev.getDamager();
                this.getPlugin().addQueryToQueue("UPDATE " + getPlugin().getDeathTable() + " "
                        + "SET amount=amount+? "
                        + "WHERE player=? "
                        + "AND cause=? "
                        + "AND entity=?",
                        "INSERT INTO " + getPlugin().getDeathTable()
                        + "(amount, player, cause, entity) VALUES "
                        + "(?,?,?,?)",
                        1, dead.getName(), a.getShooter().getType().toString().substring(0, 1)
                        + a.getShooter().getType().toString().substring(1).toLowerCase(), true);
                return;
            }
            this.getPlugin().addQueryToQueue("UPDATE " + getPlugin().getDeathTable() + " "
                    + "SET amount=amount+? "
                    + "WHERE player=? "
                    + "AND cause=?"
                    + "AND entity=?",                    
                    "INSERT INTO " + getPlugin().getDeathTable()
                    + "(amount, player, cause, entity) VALUES "
                    + "(?,?,?,?)", 1, dead.getName(), ev.getDamager().getType().toString().substring(0, 1)
                    + ev.getDamager().getType().toString().substring(1).toLowerCase(), true);
        }else{
             this.getPlugin().addQueryToQueue("UPDATE " + getPlugin().getDeathTable() + " "
                    + "SET amount=amount+? "
                    + "WHERE player=? "
                    + "AND cause=?"
                    + "AND entity=?",                    
                    "INSERT INTO " + getPlugin().getDeathTable()
                    + "(amount, player, cause, entity) VALUES "
                    + "(?,?,?,?)", 1, dead.getName(), e.getCause().toString().toUpperCase().substring(0, 1)
                    + e.getCause().toString().substring(1).toLowerCase(), true);
        }

    }

    @EventHandler(priority = EventPriority.MONITOR)
    public void logout(PlayerQuitEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        final String player = event.getPlayer().getName();
        Runnable r = new Runnable(){
            @Override
            public void run() {
               try {
                    Connection con = plugin.getMySQL().getConnection();
                    con.setAutoCommit(true);
                    PreparedStatement st = con.prepareStatement("UPDATE " + plugin.getPlayerTable() + " SET lastleave=? WHERE player=?");
                    st.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                    st.setString(2, player);
                    st.executeUpdate();
                    st.close();
                    con.close();
                    plugin.globalQueue.add(new QueryHolder("UPDATE " + plugin.getPlayerTable() + " SET lastleave=? WHERE player=?", null, new Timestamp(System.currentTimeMillis()), player));
                } catch (SQLException ex) {
                    Logger.getLogger(EventListener.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        };
        if(this.plugin.newConfig){
            this.plugin.waitings.add(r);
        }else{
            this.plugin.getServer().getScheduler().runTaskAsynchronously(plugin, r);
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void login(PlayerJoinEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        final String player = event.getPlayer().getName();
        Runnable r = new Runnable() {
            @Override
            public void run() {
                try {
                    Connection con = plugin.getMySQL().getConnection();
                    con.setAutoCommit(true);
                    PreparedStatement st = con.prepareStatement("UPDATE " + getPlugin().getPlayerTable() + " SET joins=joins+1 WHERE player=?");
                    st.setString(1, player);
                    if (st.executeUpdate() == 0) {
                        st.close();
                        st = con.prepareStatement("INSERT INTO " + getPlugin().getPlayerTable() + "(player, joins) VALUES (?, 1)");
                        st.setString(1, player);
                        st.executeUpdate();
                        st.close();
                    }
                    st = con.prepareStatement("UPDATE " + plugin.getPlayerTable() + " SET lastjoin=? WHERE player=?");
                    st.setTimestamp(1, new Timestamp(System.currentTimeMillis()));
                    st.setString(2, player);
                    st.executeUpdate();
                    st.close();
                    con.close();
                } catch (SQLException ex) {
                    Logger.getLogger(EventListener.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };
        if(this.plugin.newConfig){
            this.plugin.waitings.add(r);
        }else{
            this.plugin.getServer().getScheduler().runTaskAsynchronously(plugin, r);
        }
        plugin.globalQueue.add(new QueryHolder("UPDATE " + plugin.getPlayerTable() + " SET joins=joins+? WHERE player=?",
                "INSERT INTO " + this.getPlugin().getPlayerTable() + "(player, joins) VALUES ('" + event.getPlayer().getName() + "', 1)",
                1, event.getPlayer().getName()));
        plugin.globalQueue.add(new QueryHolder("UPDATE " + plugin.getPlayerTable() + " SET lastjoin=? WHERE player=?",
                "INSERT INTO " + plugin.getPlayerTable() + "(player, lastjoin) VALUES ('" + event.getPlayer().getName() + "', '" + new Timestamp(System.currentTimeMillis()) + "')",
                new Timestamp(System.currentTimeMillis()), event.getPlayer().getName()));
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void XPChange(PlayerExpChangeEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (event.getAmount() > 0) {
            //only add if it's positive.
            this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                    + " SET xpgained=xpgained+? WHERE player=?",
                    "INSERT INTO " + plugin.getPlayerTable() + "(xpgained, player) "
                    + "VALUES (?,?)", event.getAmount(), event.getPlayer().getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void fish(PlayerFishEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        if (event.getCaught() != null) {
            //actually something caught
            this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                    + " SET fishcatch=fishcatch+? WHERE player=?",
                    "INSERT INTO " + plugin.getPlayerTable() + " (fishcatch, player)"
                    + " VALUES (?,?)", 1, event.getPlayer().getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void kick(PlayerKickEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                + " SET timeskicked=timeskicked+? WHERE player=?",
                "INSERT INTO " + plugin.getPlayerTable() + " (timeskicked, player)"
                + " VALUES (?,?)", 1, event.getPlayer().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void eggThrow(PlayerEggThrowEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        this.getPlugin().getPlayerManager().getPlayer(event.getPlayer().getName()).getStat(StatType.EGGS_THROWN).addUpdate(new Object[]{event.getPlayer().getName()}, 1);
        /*this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                + " SET eggsthrown=eggsthrown+? WHERE player=?",
                "INSERT INTO " + plugin.getPlayerTable() + " (eggsthrown, player)"
                + " VALUES (?,?)",
                1, event.getPlayer().getName());
         */
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onEntityDamage(EntityDamageEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        Player ent = (Player) event.getEntity();
        if (!ent.hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && ent.getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                + " SET damagetaken=damagetaken+? WHERE player=?",
                "INSERT INTO " + plugin.getPlayerTable() + " (damagetaken, player)"
                + " VALUES (?,?)",
                event.getDamage(), ent.getName());
        if (event.getCause().equals(DamageCause.FIRE)) {
            this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                    + " SET onfire=onfire+? WHERE player=?",
                    "INSERT INTO " + plugin.getPlayerTable() + " (onfire, player)"
                    + " VALUES (?,?)",
                    event.getDamage(), ent.getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void itemBreak(PlayerItemBreakEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                + " SET toolsbroken=toolsbroken+? WHERE player=?",
                "INSERT INTO " + plugin.getPlayerTable() + " (toolsbroken, player)"
                + " VALUES (?,?)",
                1, event.getPlayer().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void chat(AsyncPlayerChatEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        int words = event.getMessage().split(" ").length;
        this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                + " SET wordssaid=wordssaid+? WHERE player=?", "INSERT INTO " + plugin.getPlayerTable()
                + " (wordssaid, player) VALUES (?,?)", words, event.getPlayer().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void entityShoot(EntityShootBowEvent event) {
        if (!(event.getEntity() instanceof Player)) {
            return;
        }
        if (!((Player) event.getEntity()).hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && ((Player) event.getEntity()).getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                + " SET arrows=arrows+? WHERE player=?", "INSERT INTO "
                + plugin.getPlayerTable() + " (arrows, player) VALUES "
                + "(?,?)", 1, ((Player) event.getEntity()).getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void command(PlayerCommandPreprocessEvent event) {
        if (!event.getPlayer().hasPermission(permissionNode)) {
            return;
        }
        if (this.getPlugin().getSettings().isIgnoreCreative() && event.getPlayer().getGameMode().equals(GameMode.CREATIVE)) {
            return;
        }
        this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                + " SET commandsdone=commandsdone+? WHERE player=?",
                "INSERT INTO " + plugin.getPlayerTable() + " (commandsdone, player)"
                + " VALUES (?,?)", 1, event.getPlayer().getName());
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void craftItem(CraftItemEvent event) {
        if (event.getWhoClicked() instanceof Player) {
            if (!((Player) event.getWhoClicked()).hasPermission(permissionNode)) {
                return;
            }
            if (this.getPlugin().getSettings().isIgnoreCreative() && ((Player) event.getWhoClicked()).getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }
            this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                    + " SET itemscrafted=itemscrafted+? WHERE player=?", "INSERT INTO"
                    + plugin.getPlayerTable() + " (itemscrafted, player) VALUES "
                    + "(?,?)",
                    1, ((Player) event.getWhoClicked()).getName());
        }
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void eat(FoodLevelChangeEvent event) {
        if (event.getEntity() instanceof Player) {
            if (!((Player) event.getEntity()).hasPermission(permissionNode)) {
                return;
            }
            if (this.getPlugin().getSettings().isIgnoreCreative() && ((Player) event.getEntity()).getGameMode().equals(GameMode.CREATIVE)) {
                return;
            }
            this.getPlugin().addQueryToQueue("UPDATE " + plugin.getPlayerTable()
                    + " SET omnomnom=omnomnom+? WHERE player=?", "INSERT INTO "
                    + plugin.getPlayerTable() + " (omnomnom, player) VALUES "
                    + "(?,?)", 1, ((Player) event.getEntity()).getName());
        }
    }    

    public byte getByte(int block, byte def) {
        switch (block) {
            case 23:
            case 26:
            case 27:
            case 28:
            case 29:
            case 33:
            case 50:
            case 53:
            case 54:
            case 61:
            case 62:
            case 63:
            case 64:
            case 65:
            case 66:
            case 67:
            case 71:
            case 75:
            case 76:
            case 77:
            case 85:
            case 86:
            case 91:
            case 92:
            case 93:
            case 94:
            case 95:
            case 101:
            case 102:
            case 103:
            case 104:
            case 105:
            case 106:
            case 107:
            case 108:
            case 109:
            case 111:
            case 113:
            case 114:
            case 115:
            case 117:
            case 127:
                return (byte) 0x0;
            default:
                return def;
        }
    }
}
